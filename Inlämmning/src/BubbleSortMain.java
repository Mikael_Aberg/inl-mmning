import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

//Mikael �berg

public class BubbleSortMain {
	
	public static void main(String[]args){
	
		Scanner s;
		try {
			//L�ser in texten och l�gger in den i en string array
			s = new Scanner(new File("words.txt"));
			String[] myWordList = new String[10000];
			int i = 0;
			
			int word = -1;
			while (s.hasNext()){
			    myWordList[++word] = s.next();
			i++;
			}
			
			long startTime = System.currentTimeMillis();
			
			myWordList = wordSort(myWordList);
			
			long endTime = System.currentTimeMillis();
			
			for(int j = 0; j < myWordList.length; j++){
				System.out.println(myWordList[j]);
			}
			
			System.out.println("Sortering av ord tog : " + (endTime - startTime) + " MS");
			s.close();
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//skapar en ny array av 10000 random tal mellan 0-1000
		Random r = new Random();
		int[] num = new int[10000];
		for(int i = 0; i < num.length; i++){
			num[i] = r.nextInt(1001);
		}
		
		long startTime = System.currentTimeMillis();
		
		num = numSort(num);
		
		long endTime = System.currentTimeMillis();
//		for(int i = 0; i<num.length; i++){
//			System.out.println(num[i]);
//		}
		
		System.out.println("Sortering av nummer tog : " + (endTime - startTime) + " MS");
		
	}
		
		public static int[] numSort(int [] num){
		
	     int i;
	     boolean loop = true; 
	     int numSwitch;   

	     while ( loop )
	     {
	            loop= false;    
	            for( i=0;  i < num.length -1;  i++ )
	            {
	            		if ( num[ i ] < num[i+1] )   
	                   {
	                           numSwitch = num[ i ];               
	                           num[ i ] = num[ i+1 ];
	                           num[ i+1 ] = numSwitch;
	                           loop = true;              
	                  } 
	            } 
	      } 
	     return num;
	}
		
		public static String[] wordSort(String[] myList){

		            int i;
		            boolean loop = true;
		            String switchWord;

		            while ( loop ){
		                  loop = false;
		                  //loopar igenom alla ord
		                  for ( i = 0;  i < myList.length - 1;  i++){
		                	  
		                	  		//sorterar orden i bokstavsordning genom att se om inten man f�r tillbaka av compareToIgnoreCase �r st�rre �n 0 eller ej
		                          if (myList[i].compareToIgnoreCase(myList[i + 1]) > 0){ 
		                                      switchWord = myList [ i ];
		                                      myList [ i ] = myList [ i+1];
		                                      myList [ i+1] = switchWord;
		                                      loop = true;
		                           }
		                   }
		            }
		            return myList;
		      } //wordSort
}